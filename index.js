const express = require('express')
const app = express()
const path = require('path');

let indexHTML = `<!DOCTYPE html>
<html lang="en">
		<head>
			<meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
					<meta http-equiv="X-UA-Compatible" content="ie=edge">
						<title>Document</title>
</head>
					<body>
						<h1>Hello World</h1>
					</body>
					<script src="/bootstrap-gz.js"></script>
					<script src="/bootstrap.js"></script>
					<script src=/{{scriptSrc}}></script>
</html>`;

// app.use(express.static('static'));

app.get('/', (req, res) => {
	let acceptEncoding = req.headers['accept-encoding'];
	let newHtml = '';
	if (acceptEncoding.includes('br')) {
		newHtml = indexHTML.replace(/{{scriptSrc}}/, 'bootstrap-br.js');
	} else if (acceptEncoding.includes('gzip')) {
		newHtml = indexHTML.replace(/{{scriptSrc}}/, '/bootstrap-gz.js');
	} else	{
		newHtml = indexHTML.replace(/{{scriptSrc}}/, '/bootstrap.js');
	}
	res.send(newHtml);
});

app.get('/bootstrap-br.js', (req, res) => {
	res.setHeader('Content-Encoding', 'br');
	res.sendFile(path.resolve('./static/bootstrap-br.js'));
});

app.get('/bootstrap-gz.js', (req, res) => {
	res.setHeader('Content-Encoding', 'gzip');
	res.sendFile(path.resolve('./static/bootstrap-gz.js'));
});

app.get('/bootstrap.js', (req, res) => {
	res.sendFile(path.resolve('./static/bootstrap.js'));
});

app.listen(3000, () => console.log('Example app listening on port 3000!'))